﻿using DAL.Models;
using Domain.Songs;

namespace Infrastructure.Songs;

public static class Convertor
{
public static SongModel ToDomain(this Song source) {
    return new SongModel(
            source.Id,
            source.Name,
            source.CreatorId,
            source.Duration,
            source.IsPremium,
            source.Payload,
            source.ImageId,
            source.Likes,
            source.Image.Raw,
            source.Creator.Email
    );
} 

public static IEnumerable<SongModel> ToDomain(this IEnumerable<Song> source) {
    return source.Select(ToDomain);
}

public static Song ToDal(this SongModel source) {
    return new Song() {
        Id = source.Id,
        Name = source.Name,
        CreatorId = source.CreatorId,
        IsPremium = source.IsPremium,
        Duration = source.Duration,
        Payload = source.Payload,
        Likes = source.Likes,
        ImageId = source.ImageId,
    };
}
}