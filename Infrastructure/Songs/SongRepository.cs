﻿using DAL.Models;
using Database.DataBaseContext;
using Domain.Songs;
using Microsoft.EntityFrameworkCore;


namespace Infrastructure.Songs;

public class SongRepository : ISongRepository
{
    private readonly IDataBaseContext _context;

    public SongRepository(IDataBaseContext context) {
        _context = context;
    }

    public async Task<IEnumerable<SongModel>> GetAll() {
        return (await _context.Set<Song>()
                        .Include(x => x.Image)
                        .Include(x => x.Creator)
                        .ToListAsync())
                .ToDomain();
    }

    public async Task<SongModel> GetById(Guid userId) {
        return (await _context.Set<Song>()
                        .Include(x => x.Image)
                        .Include(x => x.Creator)
                        .FirstOrDefaultAsync(x => x.Id == userId))
                !.ToDomain();
    }

    public async Task<Guid> Create(SongModel songModel) {
        var userDal = songModel.ToDal();
        _context.Add(userDal);
        await _context.SaveChangesAsync();
        return userDal.Id;
    }

    public async Task<Guid> Update(SongModel songModel) {
        var userDal = songModel.ToDal();
        _context.Update(userDal);
        await _context.SaveChangesAsync();
        return userDal.Id;
    }

    public async Task<Guid> DeleteById(Guid userId) {
        _context.Remove<Song>(userId);
        await _context.SaveChangesAsync();
        return userId;
    }
}