﻿using Domain.Users;

namespace Infrastructure.Users;

using SysUser = DAL.Models.User;

public static class Convertor
{
    public static UserModel ToDomain(this SysUser source) {
        return new UserModel(
                source.Id,
                source.NickName,
                source.Email,
                source.Password,
                source.IsSSO,
                source.IsAuthor,
                source.DateOfRegistration,
                source.LastEntered
        );
    } 
    
    public static IEnumerable<UserModel> ToDomain(this IEnumerable<SysUser> source) {
        return source.Select(ToDomain);
    }

    public static SysUser ToDal(this UserModel source) {
        return new SysUser() {
            Id = source.Id,
            NickName = source.NickName,
            Email = source.Email,
            Password = source.Password,
            IsSSO = source.IsSSO,
            DateOfRegistration = source.DateOfRegistration,
            LastEntered = source.LastEntered
        };
    }
    
}