﻿using Database.DataBaseContext;
using Domain.Users;
using Microsoft.EntityFrameworkCore;
using SysUser = DAL.Models.User;

namespace Infrastructure.Users;

public class UsersRepository : IUsersRepository
{
    private readonly IDataBaseContext _context;

    public UsersRepository(IDataBaseContext context) {
        _context = context;
    }

    public async Task<IEnumerable<UserModel>> GetAll() {
        return (await _context.Set<SysUser>().ToListAsync()).ToDomain();
    }

    public async Task<UserModel> GetById(Guid userId) {
        return (await _context.Set<SysUser>().FirstOrDefaultAsync(x=>x.Id == userId))!.ToDomain();
    }

    public async Task<Guid> Create(UserModel userModel) {
        var userDal = userModel.ProtectPassword().ToDal();
        _context.Add(userDal);
        await _context.SaveChangesAsync();
        return userDal.Id;
    }

    public async Task<Guid> Update(UserModel userModel) {
        var userDal = userModel.ProtectPassword().ToDal();
        _context.Update(userDal);
        await _context.SaveChangesAsync();
        return userDal.Id;
    }

    public async Task<Guid> DeleteById(Guid userId) {
        _context.Remove<SysUser>(userId);
        await _context.SaveChangesAsync();
        return userId;
    }
}