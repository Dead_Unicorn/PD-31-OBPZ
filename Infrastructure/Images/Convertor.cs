﻿using DAL.Models;
using Domain.Images;

namespace Infrastructure.Images;

public static class Convertor
{
    public static ImageModel ToDomain(this Image source) {
        return new ImageModel(
                source.Id,
                source.Raw,
                source.Sha256
        );
    } 
    
    public static IEnumerable<ImageModel> ToDomain(this IEnumerable<Image> source) {
        return source.Select(ToDomain);
    }

    public static Image ToDal(this ImageModel source) {
        return new Image() {
            Id = source.Id,
            Raw = source.Raw,
            Sha256 = source.Sha256,
        };
    }
}