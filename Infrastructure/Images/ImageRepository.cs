﻿using DAL.Models;
using Database.DataBaseContext;
using Domain.Images;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Images;

public class ImageRepository : IImageRepository
{
    private readonly IDataBaseContext _context;

    public ImageRepository(IDataBaseContext context) {
        _context = context;
    }

    public async Task<IEnumerable<ImageModel>> GetAll() {
        return (await _context.Set<Image>().ToListAsync()).ToDomain();
    }

    public async Task<ImageModel> GetById(Guid userId) {
        return (await _context.Set<Image>().FirstOrDefaultAsync(x=>x.Id == userId))!.ToDomain();
    }

    public async Task<Guid> Create(ImageModel imageModel) {
        var userDal = imageModel.SetHash().ToDal();
        _context.Add(userDal);
        await _context.SaveChangesAsync();
        return userDal.Id;
    }

    public async Task<Guid> Update(ImageModel imageModel) {
        var userDal = imageModel.SetHash().ToDal();
        _context.Update(userDal);
        await _context.SaveChangesAsync();
        return userDal.Id;
    }

    public async Task<Guid> DeleteById(Guid userId) {
        _context.Remove<Image>(userId);
        await _context.SaveChangesAsync();
        return userId;
    }
}