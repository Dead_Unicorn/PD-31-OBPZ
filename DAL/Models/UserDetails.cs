﻿using DAL.Models.Bases;
using Microsoft.EntityFrameworkCore;

namespace DAL.Models;

public class UserDetails : DbBase
{
    public User User { get; set; }
    public Guid UserId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
    public Guid? PhotoId { get; set; }
    public /* virtual */Image Photo { get; set; }
    public Guid? BackgroundPhotoId { get; set; }
    public /* virtual */Image BackgroundPhoto { get; set; }
}