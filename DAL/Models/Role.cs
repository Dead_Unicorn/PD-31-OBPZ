﻿using DAL.Models.Bases;

namespace DAL.Models;

public class Role : DbBase
{
    public string Name { get; set; }
    public string Description { get; set; }
}