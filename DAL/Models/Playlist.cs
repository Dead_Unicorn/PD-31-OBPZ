﻿using DAL.Models.Bases;

namespace DAL.Models;

public class Playlist : DbBase
{
    public /* virtual */User User { get; set; }
    public Guid UserId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
}