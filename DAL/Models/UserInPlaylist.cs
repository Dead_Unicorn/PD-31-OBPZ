﻿using DAL.Models.Bases;

namespace DAL.Models;

public class UserInPlaylist : DbBase
{
    public /* virtual */Playlist Playlist { get; set; }
    public Guid PlaylistId { get; set; }
    public /* virtual */User User { get; set; }
    public Guid UserId { get; set; }
}