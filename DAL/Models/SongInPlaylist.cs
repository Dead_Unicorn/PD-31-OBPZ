﻿using DAL.Models.Bases;

namespace DAL.Models;

public class SongInPlaylist :DbBase
{
    public /* virtual */Playlist Playlist { get; set; }
    public Guid PlaylistId { get; set; }
    public /* virtual */Song Song { get; set; }
    public Guid SongId { get; set; }
}