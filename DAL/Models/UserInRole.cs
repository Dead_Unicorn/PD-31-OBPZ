﻿using DAL.Models.Bases;

namespace DAL.Models;

public class UserInRole : DbBase
{
    public Guid UserId { get; set; }
    public /* virtual */User User { get; set; }
    public Guid RoleId { get; set; }
    public /* virtual */Role Role { get; set; }
}