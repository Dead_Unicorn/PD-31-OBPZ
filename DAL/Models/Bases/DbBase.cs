﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DAL.Models.Bases;

public class DbBase
{
    private Guid _id;
    
    [Key]
    public virtual Guid Id {
        get {
            if (_id == Guid.Empty){
                _id = Guid.NewGuid();
            }
            return _id;
        }
        set => _id = value;
    }
}