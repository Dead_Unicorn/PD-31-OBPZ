﻿using DAL.Models.Bases;

namespace DAL.Models;

public class Song : DbBase
{
    public string Name { get; set; }
    public Guid CreatorId { get; set; }
    public User Creator { get; set; }
    public bool IsPremium { get; set; } = false;
    public int Duration { get; set; }
    public byte[] Payload { get; set; }
    public int Likes { get; set; } = 0;
    public Guid ImageId { get; set; }
    public Image Image { get; set; }
}