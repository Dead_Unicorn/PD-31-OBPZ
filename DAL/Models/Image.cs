﻿using DAL.Models.Bases;

namespace DAL.Models;

public class Image : DbBase
{
    public byte[] Raw { get; set; }
    public string Sha256 { get; set; }
}