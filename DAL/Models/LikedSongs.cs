﻿using DAL.Models.Bases;

namespace DAL.Models;

public class LikedSong :DbBase
{
    public /* virtual */Song Song { get; set; }
    public Guid SongId { get; set; }
    public /* virtual */User User { get; set; }
    public Guid UserId { get; set; }
}