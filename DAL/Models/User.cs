﻿using DAL.Models.Bases;
using Microsoft.EntityFrameworkCore;

namespace DAL.Models;

public class User : DbBase
{
    public string? NickName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public bool IsSSO { get; set; } = false;
    public bool IsAuthor { get; set; } = false;
    public DateTime DateOfRegistration { get; set; }
    public DateTime? LastEntered { get; set; }
}