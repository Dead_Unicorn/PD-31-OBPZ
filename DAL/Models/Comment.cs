﻿using DAL.Models.Bases;

namespace DAL.Models;

public class Comment :DbBase
{
    public /* virtual */Song Song { get; set; }
    public Guid? PostId { get; set; }
    public /* virtual */Comment LinkedComment { get; set; }
    public Guid? CommentId { get; set; }
    public /* virtual */User? Commenter { get; set; }
    public Guid? CommenterId { get; set; }
    public DateTime TimeCreated { get; set; } = DateTime.Now;
    public int SongTimestamp { get; set; }
    public bool IsDeleted { get; set; }
}