﻿using Core.Interfaces;
using Database.DataBaseContext;
using Domain.Settings;

namespace Core.Services;

public class ContextManager : IContextManager
{
    private object locker = new();
    private CommonSettings _settings;

    public ContextManager(CommonSettings settings) {
        _settings = settings;
    }

    public IDataBaseContext GetContext() {
        lock (locker){
            var options = new Options() {
                ConnectionString = _settings.DataBaseConnectionString!
            };
            IDataBaseContext       _context = new DataContext(options);
            _context.Init();
            return _context;
        }
    }
    
    
}