﻿using Database.DataBaseContext;

namespace Core.Interfaces;

public interface IContextManager
{
    IDataBaseContext GetContext();
}