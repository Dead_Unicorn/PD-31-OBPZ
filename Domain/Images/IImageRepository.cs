﻿namespace Domain.Images;

public interface IImageRepository
{
    Task<IEnumerable<ImageModel>> GetAll();
    Task<ImageModel> GetById(Guid userId);
    Task<Guid> Create(ImageModel userModel);
    Task<Guid> Update(ImageModel userModel);
    Task<Guid> DeleteById(Guid userId);
}