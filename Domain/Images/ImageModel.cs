﻿using System.Security.Cryptography;

namespace Domain.Images;

public class ImageModel
{
    public ImageModel(Guid id, byte[] raw, string sha256) {
        Raw = raw;
        Sha256 = sha256;
        Id = id;
    }

    public Guid Id { get; private set; }
    public byte[] Raw { get; private set; }
    public string Sha256 { get; private set; }

    public ImageModel SetHash() {
        Sha256 = Convert.ToHexString(SHA256.HashData(Raw));
        return this;
    }
    
}