﻿namespace Domain.Songs;

public class SongModel
{
    public SongModel(Guid id, string name, Guid creatorId, int duration, bool isPremium, byte[] payload, Guid imageId, int likes, byte[] image, string creatorEmail) {
        Name = name;
        CreatorId = creatorId;
        Duration = duration;
        IsPremium = isPremium;
        Payload = payload;
        ImageId = imageId;
        Likes = likes;
        Image = image;
        CreatorEmail = creatorEmail;
        Id = id;
    }

    public Guid Id { get; private set; }
    public string Name { get; private set; }
    public Guid CreatorId { get; private set; }
    public string CreatorEmail { get; private set; }
    public bool IsPremium { get; private set; } 
    public int Duration { get; private set; }
    public byte[] Payload { get; private set; }
    public int Likes { get; private set; }
    public Guid ImageId { get; private set; }
    public byte[] Image { get; private set; }
}