﻿namespace Domain.Songs;

public interface ISongRepository
{
    Task<IEnumerable<SongModel>> GetAll();
    Task<SongModel> GetById(Guid userId);
    Task<Guid> Create(SongModel userModel);
    Task<Guid> Update(SongModel userModel);
    Task<Guid> DeleteById(Guid userId);
}