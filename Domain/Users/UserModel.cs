﻿using System.Security.Cryptography;
using System.Text;

namespace Domain.Users;

public class UserModel
{
    public UserModel(Guid id, string? nickName, string email, string password, bool isSso, bool isAuthor, DateTime dateOfRegistration, DateTime? lastEntered) {
        Id = id;
        NickName = nickName;
        Email = email;
        Password = password;
        IsSSO = isSso;
        IsAuthor = isAuthor;
        DateOfRegistration = dateOfRegistration;
        LastEntered = lastEntered;
    }

    public Guid Id { get; private set; }
    public string? NickName { get; private set; }
    public string Email { get; private set; }
    public string Password { get; private set; }
    public bool IsSSO { get; private set; }
    public bool IsAuthor { get; private set; }
    public DateTime DateOfRegistration { get; private set; }
    public DateTime? LastEntered { get; private set; }

    public UserModel ProtectPassword() {
        byte[] hash = SHA256.HashData(Encoding.UTF8.GetBytes(this.Password));
        this.Password = ToHexString(hash);
        return this;
    }

    private static string ToHexString(byte[] hex) {
        var sb = new StringBuilder(hex.Length * 2);
        foreach (byte b in hex){
            sb.AppendFormat("{0:x2}", b);
        }

        return sb.ToString();
    }
    
}