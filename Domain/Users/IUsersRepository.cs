﻿namespace Domain.Users;

public interface IUsersRepository
{
    Task<IEnumerable<UserModel>> GetAll();
    Task<UserModel> GetById(Guid userId);
    Task<Guid> Create(UserModel userModel);
    Task<Guid> Update(UserModel userModel);
    Task<Guid> DeleteById(Guid userId);
}