﻿using DAL.Models;
using Database.DataBaseContext;
using Microsoft.AspNetCore.Mvc;

namespace project.Controllers;

[Route("test")]
public class TestController : Controller
{
    private readonly IDataBaseContext _context;

    public TestController(IDataBaseContext context) {
        _context = context;
    }

    [HttpGet]
    public IActionResult Get() {
        var users = _context.Set<User>();
        return Ok(users.ToList());
    }
    
}