﻿using Domain.Songs;
using Microsoft.AspNetCore.Mvc;
using project.Dto.Songs;

namespace project.Controllers;

[Route("api/v1")]
[ApiController]
public class SongController : Controller
{
    private readonly ISongRepository _songRepository;

    public SongController(ISongRepository songRepository) {
        _songRepository = songRepository;
    }
    
    [HttpGet("songs")]
    public async Task<ActionResult<List<SongDto>>> GetAll() {
        return Ok((await _songRepository.GetAll()).ToDto());
    }
    
    [HttpGet("song/{userId:guid}")]
    public async Task<ActionResult<SongDto>> GetById(Guid userId) {
        return Ok((await _songRepository.GetById(userId)).ToDto());
    }
    
    [HttpPost("song")]
    public async Task<ActionResult<Guid>> CreateSong([FromBody]SongDto userDto) {
        Guid createdSongId = await  _songRepository.Create(userDto.ToDomain());
        return Ok(createdSongId);
    }
    
    [HttpPut("song")]
    public async Task<ActionResult<Guid>> Update([FromBody]SongDto song) {
        return Ok(await _songRepository.Update(song.ToDomain()));
    }
    
    [HttpDelete("song/{userId:guid}")]
    public async Task<ActionResult<Guid>> DeleteById(Guid userId) {
        return Ok(await _songRepository.DeleteById(userId));
    }
    
}