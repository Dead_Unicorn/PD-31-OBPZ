﻿using Domain.Images;
using Microsoft.AspNetCore.Mvc;
using project.Dto.Images;

namespace project.Controllers;

[Route("api/v1")]
[ApiController]
public class ImageController : Controller
{
    private readonly IImageRepository _imageRepository;

    public ImageController(IImageRepository imageRepository) {
        _imageRepository = imageRepository;
    }
    
    [HttpGet("images")]
    public async Task<ActionResult<List<ImageDto>>> GetAll() {
        return Ok((await _imageRepository.GetAll()).ToDto());
    }
    
    [HttpGet("image/{userId:guid}")]
    public async Task<ActionResult<ImageDto>> GetById(Guid userId) {
        return Ok((await _imageRepository.GetById(userId)).ToDto());
    }
    
    [HttpPost("image")]
    public async Task<ActionResult<Guid>> CreateImage([FromBody]ImageDto userDto) {
        Guid createdImageId = await  _imageRepository.Create(userDto.ToDomain());
        return Ok(createdImageId);
    }
    
    [HttpPut("image")]
    public async Task<ActionResult<Guid>> Update([FromBody]ImageDto image) {
        return Ok(await _imageRepository.Update(image.ToDomain()));
    }
    
    [HttpDelete("image/{userId:guid}")]
    public async Task<ActionResult<Guid>> DeleteById(Guid userId) {
        return Ok(await _imageRepository.DeleteById(userId));
    }
    
}