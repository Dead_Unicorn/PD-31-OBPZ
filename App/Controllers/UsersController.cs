﻿using Domain.Users;
using Microsoft.AspNetCore.Mvc;
using project.Dto.Users;

namespace project.Controllers;

[Route("api/v1")]
[ApiController]
public class UsersController : ControllerBase
{
    private IUsersRepository _repository;

    public UsersController(IUsersRepository repo) {
        _repository = repo;
    }

    [HttpGet("users")]
    public async Task<ActionResult<List<UserDto>>> GetAll() {
        return Ok((await _repository.GetAll()).ToDto());
    }
    
    [HttpGet("user/{userId:guid}")]
    public async Task<ActionResult<UserDto>> GetById(Guid userId) {
        return Ok((await _repository.GetById(userId)).ToDto());
    }
    
    [HttpPost("user")]
    public async Task<ActionResult<Guid>> CreateUser([FromBody]UserDto userDto) {
        Guid createdUserId = await  _repository.Create(userDto.ToDomain());
        return Ok(createdUserId);
    }
    
    [HttpPut("user")]
    public async Task<ActionResult<Guid>> Update([FromBody]UserDto user) {
        return Ok(await _repository.Update(user.ToDomain()));
    }
    
    [HttpDelete("user/{userId:guid}")]
    public async Task<ActionResult<Guid>> DeleteById(Guid userId) {
        return Ok(await _repository.DeleteById(userId));
    }
}