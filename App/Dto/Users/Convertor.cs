﻿using Domain.Users;

namespace project.Dto.Users;

public static class Convertor
{
    public static UserDto ToDto(this UserModel source) {
        return new UserDto{
                Id = source.Id,
                Name = source.NickName,
                Email = source.Email,
                Password = source.Password,
                IsSSO = source.IsSSO,
                IsAuthor = source.IsAuthor,
                RegisteredAt = source.DateOfRegistration,
                LastEntered = source.LastEntered
        };
    } 
    
    public static IEnumerable<UserDto> ToDto(this IEnumerable<UserModel> source) {
        return source.Select(ToDto);
    } 
    
    public static UserModel ToDomain(this UserDto source) {
        return new UserModel(
                source.Id,
                source.Name,
                source.Email,
                source.Password,
                source.IsSSO,
                source.IsAuthor,
                source.RegisteredAt,
                source.LastEntered
        );
    } 
    
    public static IEnumerable<UserModel> ToDomain(this IEnumerable<UserDto> source) {
        return source.Select(ToDomain);
    } 
    
}