﻿namespace project.Dto.Users;

public class UserDto
{
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsSSO { get; set; }
        public bool IsAuthor { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime? LastEntered { get; set; }
}

