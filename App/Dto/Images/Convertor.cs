﻿using Domain.Images;

namespace project.Dto.Images;

public static class Convertor
{
    public static ImageDto ToDto(this ImageModel source) {
        return new ImageDto{
            Id = source.Id,
            RawBase64 = Convert.ToBase64String(source.Raw),
            Sha256 = source.Sha256
        };
    } 
    
    public static IEnumerable<ImageDto> ToDto(this IEnumerable<ImageModel> source) {
        return source.Select(ToDto);
    } 
    
    public static ImageModel ToDomain(this ImageDto source) {
        return new ImageModel(
                source.Id,
                source.RawBase64 is null ? new byte[]{} : Convert.FromBase64String(source.RawBase64),
                source.Sha256
        );
    } 
    
    public static IEnumerable<ImageModel> ToDomain(this IEnumerable<ImageDto> source) {
        return source.Select(ToDomain);
    } 
}