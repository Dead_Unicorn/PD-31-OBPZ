﻿namespace project.Dto.Images;

public class ImageDto
{
    public Guid Id { get;  set; }
    public string RawBase64 { get;  set; }
    public string Sha256 { get;  set; }
}