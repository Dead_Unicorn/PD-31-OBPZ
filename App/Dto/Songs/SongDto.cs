﻿namespace project.Dto.Songs;

public class SongDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Guid CreatorId { get; set; }
    public string? CreatorEmail { get; set; }
    public bool IsPremium { get; set; } 
    public int Duration { get; set; }
    public string PayloadBase64 { get; set; }
    public int Likes { get; set; }
    public Guid ImageId { get; set; }
    public string? ImageBase64 { get; set; }
}