﻿using Domain.Songs;

namespace project.Dto.Songs;

public static class Convertor
{
    public static SongDto ToDto(this SongModel source) {
        return new SongDto{
            Id = source.Id,
            Name = source.Name,
            CreatorId = source.CreatorId,
            IsPremium = source.IsPremium,
            Duration = source.Duration,
            PayloadBase64 = Convert.ToBase64String(source.Payload),
            Likes = source.Likes,
            ImageId = source.ImageId,
            CreatorEmail = source.CreatorEmail,
            ImageBase64 = Convert.ToBase64String(source.Image)
        };
    } 
    
    public static IEnumerable<SongDto> ToDto(this IEnumerable<SongModel> source) {
        return source.Select(ToDto);
    } 
    
    public static SongModel ToDomain(this SongDto source) {
        return new SongModel(
                source.Id,
                source.Name,
                source.CreatorId,
                source.Duration,
                source.IsPremium,
                source.PayloadBase64 is null ? new byte[]{} : Convert.FromBase64String(source.PayloadBase64),
                source.ImageId,
                source.Likes,
                null,
                null
        );
    } 
    
    public static IEnumerable<SongModel> ToDomain(this IEnumerable<SongDto> source) {
        return source.Select(ToDomain);
    } 
}