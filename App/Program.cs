using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using project;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.ConfigureServices();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSpaStaticFiles();
app.UseCors("PermitAll");
app.UseRouting();
app.UseAuthorization();
app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
// app.MapControllers();

// app.MapRazorPages();
app.UseSpa(config => {
    // if (app.Environment.IsDevelopment())
    // {
    //     config.UseProxyToSpaDevelopmentServer("http://localhost:8080");
    // }
});
app.MapGet("health", async context => {
   await context.Response.WriteAsync("Healthy!");
});
app.Run();
