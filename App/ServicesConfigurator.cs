﻿using Core.Interfaces;
using Core.Services;
using Database.DataBaseContext;
using Domain.Images;
using Domain.Settings;
using Domain.Songs;
using Domain.Users;
using Infrastructure.Images;
using Infrastructure.Songs;
using Infrastructure.Users;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace project;

internal static class ServicesConfigurator
{
    public static WebApplicationBuilder ConfigureServices(this WebApplicationBuilder builder) {
        var services = builder.Services;
        CommonSettings settings = new CommonSettings();
        builder.Configuration.Bind(nameof(CommonSettings),settings);
        
        services.AddControllers();
        services.AddSpaStaticFiles(config => {
            config.RootPath = "frontend/dist";
        });
        services.AddCors(config => {
            config.AddPolicy("PermitAll", policyBuilder => {
                policyBuilder.AllowAnyHeader();
                policyBuilder.AllowAnyOrigin();
                policyBuilder.AllowAnyMethod();
            });
        });
        // services.AddRazorPages();
        services.AddSingleton<CommonSettings>(settings);
        services.AddSingleton<IContextManager, ContextManager>();
        services.AddTransient<IDataBaseContext>(services => {
            var manager = services.GetRequiredService<IContextManager>();
            return manager.GetContext();
        });
        services.AddTransient<IUsersRepository, UsersRepository>();
        services.AddTransient<IImageRepository, ImageRepository>();
        services.AddTransient<ISongRepository, SongRepository>();
        return builder;
    }
}
