import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import withUUID from "vue-uuid";

withUUID(createApp(App)).use(router).mount("#app");
