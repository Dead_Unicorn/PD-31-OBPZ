﻿import {client} from './client';

export default {
    getAll() {
        return client.get("users");
    },
    getById(userId) {
        return client.get(`user/${userId}`)
    },
    createUser(user) {
        // return client.post('user', JSON.stringify({userDto:user}));
        return client.post('user', JSON.stringify(user), {
            headers: {
                "Content-Type": "application/json"
            }
        });
    },
    updateUser(user) {
        return client.put('user', JSON.stringify(user), {
            headers: {
                "Content-Type": "application/json"
            }
        });
    },
    deleteById(userId) {
        return client.delete(`user/${userId}`);
    }
}
