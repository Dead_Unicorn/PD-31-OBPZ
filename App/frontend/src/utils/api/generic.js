﻿import {client} from "@/utils/api/client";

export default class GenericClient{
    constructor(url) {
        this.url = url;
    }
    init(url){
        this.url = url;
    }
    validate(){
      if (!this.url){
          throw new Error("set the url first");
      }  
    }
    getAll() {
        return client.get(`${this.url}s`);
    }
    getById(userId) {
        return client.get(`${this.url}/${userId}`)
    }
    create(user) {
        return client.post(`${this.url}`, JSON.stringify(user), {
            headers: {
                "Content-Type": "application/json"
            }
        });
    }
    update(user) {
        return client.put(`${this.url}`, JSON.stringify(user), {
            headers: {
                "Content-Type": "application/json"
            }
        });
    }
    deleteById(userId) {
        return client.delete(`${this.url}/${userId}`);
    }
}