﻿import axios from "axios";

let httpsUrl = 'https://localhost:7106';
// let httpUrl = 'http://localhost:5186';
// let baseUrl = '';
// let res = await axios.get(httpsUrl + '/health');
// if(res.status === 200){
//     baseUrl = httpsUrl;
// }else{
//     res = await axios.get(httpUrl + '/health');
//     if(res.status === 200){
//         baseUrl = httpUrl;
//     }else{
//         throw new Error('can\'t connect');
//     }
// }

export const client = axios.create({
    baseURL: `${httpsUrl}/api/v1/`,
    json: true,
})


