﻿import GenericClient from "@/utils/api/generic";

const imageClient = new GenericClient('image');
export default imageClient;