﻿export function utf8_to_b64(str) {
    let res = null;
    try {
        res = window.btoa(unescape(encodeURIComponent(str)));
    } catch (e) {
        console.log(e);
    }
    return res ?? '';
}

export function b64_to_utf8(str) {
    let res = null;
    try {
        res = decodeURIComponent(escape(window.atob(str)));
    } catch (e) {
        console.log(e);
    }
    return res ?? '';
}