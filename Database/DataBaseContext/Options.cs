﻿using Microsoft.EntityFrameworkCore;

namespace Database.DataBaseContext;

public class Options
{
    public string ConnectionString { get; set; }
    public ServerVersion ServerVersion { get; set; } = new MySqlServerVersion(new Version(8, 0));
}