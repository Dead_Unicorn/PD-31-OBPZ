﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace Database.DataBaseContext;

public class DesignTimeFactory : IDesignTimeDbContextFactory<DataContext>
{
    public DataContext CreateDbContext(string[] args)
    {
        // var optionsBuilder = new DbContextOptionsBuilder<BloggingContext>();
        // optionsBuilder.UseMySql("Host=localhost;Port=3307;Database=dutovpz;User=root;Password=password;Pooling=True;Convert zero datetime=True;Charset=utf8", ServerVersion.Create(8,0,8, ServerType.MySql));
        return new DataContext();
    }
}