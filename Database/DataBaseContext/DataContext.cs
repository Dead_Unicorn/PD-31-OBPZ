﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.DataBaseContext;

public class DataContext : DataBaseContext
{
    public DataContext(Options options): base(options) {
    }

    public DataContext() :base(new Options(){ConnectionString = "Host=localhost;Port=3307;Database=dutovpz;User=root;Password=password;Pooling=True;Convert zero datetime=True;Charset=utf8"}) {
        
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
        optionsBuilder
                // .UseLazyLoadingProxies()
                .UseMySql(ConnectionString, Version, options => {
                    options.EnableRetryOnFailure(5);
                });
    }

    public virtual DbSet<Image>? Images { get; set; }
    public virtual DbSet<Comment>? Comments { get; set; }
    public virtual DbSet<LikedSong>? LikedSongs { get; set; }
    public virtual DbSet<Playlist>? Playlists { get; set; }
    public virtual DbSet<Role>? Roles { get; set; }
    public virtual DbSet<Song>? Songs { get; set; }
    public virtual DbSet<SongInPlaylist>? SongsInPlaylists { get; set; }
    public virtual DbSet<User>? Users { get; set; }
    public virtual DbSet<UserDetails>? UserDetails { get; set; }
    public virtual DbSet<UserInPlaylist>? UsersInPlaylists { get; set; }
    public virtual DbSet<UserInRole>? UsersInRoles { get; set; }
}