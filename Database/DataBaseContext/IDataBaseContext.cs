﻿using DAL.Models.Bases;

namespace Database.DataBaseContext;

public interface IDataBaseContext
{
    IQueryable<T> Set<T>() where T : DbBase;
    void Remove<T>(Guid id) where T : DbBase, new();
    void Add<T>(T obj) where T : DbBase;
    void Update<T>(T obj) where T : DbBase;
    Task SaveChangesAsync();
    void Init();
}