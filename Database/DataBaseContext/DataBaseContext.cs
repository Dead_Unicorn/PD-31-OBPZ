﻿using DAL.Models;
using DAL.Models.Bases;
using Microsoft.EntityFrameworkCore;

namespace Database.DataBaseContext;

public class DataBaseContext : DbContext, IDataBaseContext
{
    public DataBaseContext(Options options) {
        // if (!IsReady){
        //     this.Database.Migrate();    
        // }
        Version = (MySqlServerVersion)options.ServerVersion;
        ConnectionString = options.ConnectionString;
    }

    protected string ConnectionString;
    protected MySqlServerVersion Version;
    private bool IsReady = false;
    
    
    public IQueryable<T> Set<T>() where T : DbBase {
        return base.Set<T>();
    }

    public void Remove<T>(Guid id) where T : DbBase, new() {
        base.Remove(new T{Id=id});
    }

    public void Add<T>(T obj) where T : DbBase {
        base.Add(obj);
    }

    public void Update<T>(T obj) where T : DbBase {
        base.Update(obj);
    }

    public Task SaveChangesAsync() {
        return base.SaveChangesAsync();
    }


    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<User>()
                .HasIndex(nameof(User.Email))
                .IsUnique();
        modelBuilder.Entity<Song>()
                .HasIndex(nameof(Song.CreatorId), nameof(Song.Name))
                .IsUnique();
        modelBuilder.Entity<Song>()
                .HasIndex(nameof(Song.CreatorId), nameof(Song.Name));
        modelBuilder.Entity<UserDetails>()
                .HasIndex(nameof(UserDetails.UserId));

    }

    
    public void Init() {
        if (IsReady)
            return;
        Database.Migrate();
        IsReady = true;
    }
}